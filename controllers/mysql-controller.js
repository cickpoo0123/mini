const mysql = require('mysql');
const connection = mysql.createConnection({
      host: 'mysql',
      user: 'dev',
      password: '1234',
      database : 'app'
});

connection.query('SELECT 1', function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }
    console.log('connected!');
});

function mysqlSelect() {
    var sql    = 'SELECT * FROM customers'

    connection.query(sql, function(err, results) {
        if (err){ 
            throw err;
        }
        console.log(results[0]);
        return results[0];
    });
}

module.exports = {
	mysqlSelect:mysqlSelect
};